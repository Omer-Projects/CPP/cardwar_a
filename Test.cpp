// Entry point for testing

#include <cstdlib>
#include <iostream>
#include <string.h>

#include "doctest.h"

#include "sources/card.hpp"
#include "sources/game.hpp"
#include "sources/player.hpp"

#define NUMBER_OF_SETS 2

using namespace doctest;
using namespace ariel;

/**
 *  If anybody reads this file, this is not good testing.
 * The game is random and cannot add mocking and we cannot test new functions (getters for example) to get more information.
 * So, We cannot create complete logic tests with fake data and known expected results.
 */

TEST_CASE("build")
{
    Player player1("Clark");
    Player player2("Lex");

    Game game(player1, player2);

    bool complexCheck = player1.stacksize() == 26 && player2.stacksize() == 26;
    CHECK(complexCheck);

    complexCheck = player1.cardesTaken() == 0 && player2.cardesTaken() == 0;
    CHECK(complexCheck);
}

TEST_CASE("run a random game")
{
    Player player1("Clark");
    Player player2("Lex");

    Game game(player1, player2);

    bool complexCheck = player1.stacksize() == 26 && player2.stacksize() == 26;
    CHECK(complexCheck);

    complexCheck = player1.cardesTaken() == 0 && player2.cardesTaken() == 0;
    CHECK(complexCheck);

    game.playAll();

    complexCheck = player1.stacksize() == 0 && player2.stacksize() == 0;
    CHECK(complexCheck);

    game.printLog();
    game.printStats();
    game.printWiner();
}

TEST_CASE("Multi games")
{
    Player player1("Clark");
    Player player2("Lex");

    Game game1(player1, player2);

    CHECK(player1.stacksize() == 26);
    CHECK(player2.stacksize() == 26);
    game1.playAll();
    CHECK(player1.stacksize() == 0);
    CHECK(player2.stacksize() == 0);

    Game game2(player2, player1);

    CHECK(player1.stacksize() == 26);
    CHECK(player2.stacksize() == 26);

    game2.playTurn();

    CHECK(player1.stacksize() + player1.cardesTaken() + player2.stacksize() + player2.cardesTaken() == 52);

    if (player1.stacksize() > 0) {
        CHECK(player1.cardesTaken() + player2.cardesTaken() > 0);
    }

    game2.playAll();

    CHECK(player1.stacksize() == 0);
    CHECK(player2.stacksize() == 0);
}

TEST_CASE("Catch exceptions")
{
    Player player1("Clark");
    Player player2("Lex");
    Player player3("Lionel");

    Game game1(player1, player2);

    CHECK(player1.isPlaying());
    CHECK(player2.isPlaying());
    CHECK(!player3.isPlaying());

    bool exceptionCatched = false;
    try {
        game1.printLastTurn();
    } catch (std::exception& ex) {
        exceptionCatched = true;
    }

    CHECK(exceptionCatched);

    exceptionCatched = false;
    try {
        game1.printWiner();
    } catch (std::exception& ex) {
        exceptionCatched = true;
    }

    CHECK(exceptionCatched);

    exceptionCatched = false;
    try {
        Game game2(player2, player3);
    } catch (std::exception& ex) {
        exceptionCatched = true;
    }

    CHECK(exceptionCatched);

    exceptionCatched = false;
    try {
        Game game2(player3, player3);
    } catch (std::exception& ex) {
        exceptionCatched = true;
    }

    CHECK(exceptionCatched);

    game1.playAll();

    exceptionCatched = false;
    try {
        game1.playTurn();
    } catch (std::exception& ex) {
        exceptionCatched = true;
    }

    CHECK(exceptionCatched);
}

TEST_CASE("Full Game")
{
    Player player1("Clark");
    Player player2("Lex");

    Game game(player1, player2);

    game.playTurn();

    game.playAll();

    game.printLog();
    game.printStats();
    game.printWiner();
}
