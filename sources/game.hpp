// Game play class
#ifndef GAME_H
#define GAME_H

#include <exception>
#include <iostream>
#include <string>
#include <vector>

#include "card.hpp"
#include "player.hpp"

namespace ariel {
/**
 * A single war game between two players
 */
class Game {
private:
    /**
     * A single turn log data
     */
    class TurnData {
    public:
        std::vector<Card> m_player1Cards;
        std::vector<Card> m_player2Cards;
        int m_winer = 0;

        TurnData()
        {
        }
    };

    Player& m_player1;
    Player& m_player2;

    unsigned long m_stage = 0;
    std::vector<Card> m_allCards;

    int m_player1Wins = 0;
    int m_player2Wins = 0;
    int m_amountOfDraws = 0;
    std::vector<TurnData> m_turnsData;

public:
    Game() = delete;

    /**
     * Create a game with random start
     *
     * @param player1 player 1
     * @param player2 player 2
     */
    Game(Player& player1, Player& player2)
        : m_player1(player1)
        , m_player2(player2)
    {
        if (player1.isPlaying() || player2.isPlaying()) {
            throw std::runtime_error("Error: Players are not available");
        }

        if (player1.getID() == player2.getID()) {
            // throw std::runtime_error("Error: Player cannot play against himself");
            // How player can play against himself ?
        }

        this->init();
    }

    /**
     * Play a single turn
     */
    void playTurn();

    /**
     * Play's the game until the end
     */
    void playAll();

    /**
     * Print the last turn stats.
     * For Example, without draws:
     *      Alice played Queen of Hearts Bob played 5 of Spades. Alice wins.
     * With draws (print in single line):
     *      Alice played 6 of Hearts Bob played 6 of Spades. Draw.
     *      Alice played 10 of Clubs Bob played 10 of Diamonds. draw.
     *      Alice played Jack of Clubs Bob played King of Diamonds. Bob wins.
     */
    void printLastTurn() const;

    /**
     * Prints the name of the winning player
     */
    void printWiner() const;

    /**
     * Prints all the turns played one line per turn
     *
     * @note Use the same format as printLastTurn
     */
    void printLog() const;

    /**
     * For each player prints basic statistics: win rate, cards won.
     * Also print the draw rate and amount of draws that happen.
     *
     * @note Draw within a draw counts as 2 draws.
     */
    void printStats() const;

private:
    /**
     * Initialize the game
     */
    void init();

    /**
     * Print single turn as log
     * @param turnData turn data
     */
    void printTurn(const TurnData& turnData) const;
};
} // namespace ariel

using namespace ariel;

#endif
