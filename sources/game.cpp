#include "game.hpp"

#include <cmath>

namespace ariel {
void Game::playTurn()
{
    // How player can play against himself ? - this need to be stopped in the constructor.
    if (m_player1.getID() == m_player2.getID()) {
        throw std::runtime_error("Error: Player cannot play against himself");
    }

    if (m_stage >= 26) {
        throw std::runtime_error("Error: The game is ended");
    }

    TurnData turnData;

    int playerWon = 0;
    unsigned long start = m_stage;
    while (playerWon == 0 && m_stage < 26) {
        Card card1 = m_allCards.at(m_stage * 2);
        Card card2 = m_allCards.at(m_stage * 2 + 1);

        turnData.m_player1Cards.push_back(card1);
        turnData.m_player2Cards.push_back(card2);

        if (card2 < card1) {
            playerWon = 1;
            m_stage++;
        } else if (card1 < card2) {
            playerWon = 2;
            m_stage++;
        } else {
            m_amountOfDraws += 1;
            m_stage += 2;
        }
    }

    if (playerWon != 0) {
        turnData.m_winer = playerWon;

        if (playerWon == 1) {
            m_player1.takeCards(2 * (m_stage - start));
            m_player1Wins++;
        } else {
            m_player2.takeCards(2 * (m_stage - start));
            m_player2Wins++;
        }
        m_player1.setStackSize(26 - m_stage);
        m_player2.setStackSize(26 - m_stage);
    } else {
        m_player1.takeCards(26 - start);
        m_player2.takeCards(26 - start);
        m_player1.setStackSize(0);
        m_player2.setStackSize(0);
    }

    m_turnsData.push_back(turnData);

    if (m_stage >= 26) {
        m_player1.endGame();
        m_player2.endGame();
    }
}

void Game::playAll()
{
    while (m_stage < 26) {
        this->playTurn();
    }
}

void Game::printLastTurn() const
{
    if (m_turnsData.empty()) {
        throw std::runtime_error("Error: their was no any turn");
    }

    printTurn(m_turnsData.at(m_turnsData.size() - 1));
}

void Game::printWiner() const
{
    if (m_stage < 26) {
        // throw std::runtime_error("Error: The game is not ended");
        // If the game is ended it need to throw exception and not continue.
        return;
    }

    if (m_player1.cardesTaken() > m_player2.cardesTaken()) {
        std::cout << "The player " << m_player1.getName() << " wins" << std::endl;
    } else if (m_player1.cardesTaken() < m_player2.cardesTaken()) {
        std::cout << "The player " << m_player2.getName() << " wins" << std::endl;
    } else {
        // std::cout << "No player as win. they both got " << m_player1.cardesTaken() << " cards" << std::endl;
        throw std::runtime_error("Error: tie is exception ... ");
        // why tie is exception ?
    }
}

void Game::printLog() const
{
    for (auto turnData : m_turnsData) {
        printTurn(turnData);
    }
}

void Game::printStats() const
{
    double all = m_player1Wins + m_player2Wins + m_amountOfDraws;

    double player1WinRate = (double)m_player1Wins / all;
    player1WinRate = round(player1WinRate * 100.0);

    double player2WinRate = (double)m_player2Wins / all;
    player2WinRate = round(player2WinRate * 100.0);

    double drawsRate = (double)m_amountOfDraws / all;
    drawsRate = round(drawsRate * 100.0);

    std::cout << "Player " << m_player1.getName() << " wins: " << m_player1Wins << " (" << player1WinRate << "% rate) " << m_player1.cardesTaken() << " cards won" << std::endl;
    std::cout << "Player " << m_player2.getName() << " wins: " << m_player2Wins << " (" << player2WinRate << "% rate) " << m_player2.cardesTaken() << " cards won" << std::endl;
    std::cout << "Was " << m_amountOfDraws << " (" << drawsRate << "% rate) draws" << std::endl;
}

// private
void Game::init()
{
    std::vector<Card> allCards;

    for (int i = 1; i <= 13; i++) {
        allCards.push_back(Card(i, CardType::club));
        allCards.push_back(Card(i, CardType::diamond));
        allCards.push_back(Card(i, CardType::heart));
        allCards.push_back(Card(i, CardType::spade));
    }

    while (!allCards.empty()) {
        m_allCards.push_back(allCards.at((unsigned long)randInteger(0, allCards.size() - 1)));
        allCards.pop_back();
        m_allCards.push_back(allCards.at((unsigned long)randInteger(0, allCards.size() - 1)));
        allCards.pop_back();
    }

    m_player1.startGame();
    m_player2.startGame();
}

void Game::printTurn(const TurnData& turnData) const
{
    for (unsigned long i = 0; i < turnData.m_player1Cards.size(); i++) {
        std::cout << m_player1.getName() << " played ";
        turnData.m_player1Cards[i].print();
        std::cout << " " << m_player2.getName() << " played ";
        turnData.m_player2Cards[i].print();
        if (turnData.m_player1Cards[i].getNumber() == turnData.m_player2Cards[i].getNumber()) {
            std::cout << ", Draw." << std::endl;
        } else {
            if (turnData.m_winer == 1) {
                std::cout << ", " << m_player1.getName() << " wins." << std::endl;
            } else {
                std::cout << ", " << m_player2.getName() << " wins." << std::endl;
            }
        }
    }
}

}
