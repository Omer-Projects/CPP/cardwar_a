// Player class
#ifndef PLAYER_H
#define PLAYER_H

#include <string>

#include "card.hpp"

namespace ariel {
class Player {
private:
    const char* m_name;
    const int m_ID;

    bool m_playing = false;
    int m_stackSize = 0;
    int m_cardsTaken = 0;

public:
    /**
     * Create a player
     * @param name player name
     */
    Player(const char* name);

    /**
     * get player name
     * @return player name
     */
    inline const char* getName() const { return m_name; }

    /**
     * get player ID
     * @return player ID
     */
    inline int getID() const { return m_ID; }

    /**
     * Check if the player play right now
     * @return the check
     */
    inline bool isPlaying() const { return m_playing; }

    /**
     * Event when the game started
     * @param cards the cards for the game
     */
    void startGame();

    /**
     * Event when the game ended
     * @param cards the cards for the game
     */
    void endGame();

    /**
     * Return the amount of cards left to the player
     *
     * @return amount of cards
     */
    int stacksize() const { return m_stackSize; }

    /**
     * Return the amount of cards the player has won
     *
     * @return amount of cards
     */
    int cardesTaken() const { return m_cardsTaken; }

    /**
     * Add won cards to the player
     * @param cardsCount amount of new cards
     */
    void takeCards(int cardsCount) { m_cardsTaken += cardsCount; }

    /**
     * Set Stack Size
     * @param stackSize new Stack Size
     */
    void setStackSize(int stackSize) { m_stackSize = stackSize; }
};
} // namespace ariel
#endif
