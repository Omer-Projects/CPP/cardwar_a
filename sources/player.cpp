#include "player.hpp"

namespace ariel {

// static variables
static int s_playersCount = 0;

Player::Player(const char* name)
    : m_name(name)
    , m_ID(s_playersCount++)
{
}

void Player::startGame()
{
    m_playing = true;
    m_stackSize = 26;
    m_cardsTaken = 0;
}

void Player::endGame()
{
    m_playing = false;
}
}