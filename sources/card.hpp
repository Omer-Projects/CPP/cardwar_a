// Card class
#ifndef CARD_H
#define CARD_H

#include <string>

namespace ariel {

/**
 * Rand a integer between min and max
 * @param min min number
 * @param max max number
 * @return random integer between min and max
 */
int randInteger(int min, int max);

enum CardType {
    club = 0,
    diamond = 1,
    heart = 2,
    spade = 3
};

/**
 * War Game Card
 */
class Card {
    // members
private:
    const int m_number;
    const CardType m_type;

public:
    Card() = delete;

    /**
     * Create new Card
     * @param number card number
     * @param type card type
     * @note 1 - As, 11 - Prince, 12 - Queen, 13 - King
     */
    Card(int number, CardType type)
        : m_number(number)
        , m_type(type)
    {
    }

    bool operator<(const Card& other);

    /**
     * Get card number
     * @note 1 - As, 11 - Prince, 12 - Queen, 13 - King / Joker
     * @return card number
     */
    inline int getNumber() const { return m_number; }

    /**
     * Get the type of the card
     * @return card type
     */
    inline CardType getType() const { return m_type; }

    /**
     * Print the card
     * Example: King of Diamonds
     */
    void print() const;
};
} // namespace ariel

#endif
