#include "card.hpp"

#include <cstdlib>
#include <iostream>

namespace ariel {

static bool s_randInitialized = false;

int randInteger(int min, int max)
{
    if (!s_randInitialized) {
        std::srand((unsigned)time(NULL));
        s_randInitialized = true;
    }

    return (int)(std::rand() % (max - min + 1)) + min;
}

bool Card::operator<(const Card& other)
{
    if (other.m_number == 1) {
        return m_number >= 3;
    }
    if (m_number == 1) {
        return other.m_number == 2;
    }
    return other.m_number > m_number;
}

void Card::print() const
{
    if (1 < m_number && m_number < 11) {
        std::cout << m_number;
    } else {
        switch (m_number) {
        case 1: {
            std::cout << "As";
        } break;
        case 11: {
            std::cout << "Prince";
        } break;
        case 12: {
            std::cout << "Queen";
        } break;
        case 13: {
            std::cout << "King";
        } break;
        }
    }
    std::cout << " of ";
    if (m_type == CardType::club) {
        std::cout << "Clubs";
    } else if (m_type == CardType::diamond) {
        std::cout << "Diamonds";
    } else if (m_type == CardType::heart) {
        std::cout << "Hearts";
    } else {
        std::cout << "Spades";
    }
}
}